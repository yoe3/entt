#include <catch2/catch_test_macros.hpp>
#include <yoe/actor.hh>
#include <yoe/world.hh>
#include <yoe/components/transform.hh>

// actor_type objects cannot be created they do not satisfy the needed concepts
// instead we use this test_actor_type
class test_actor_type: public yoe::actor_type
{
public:
    struct tag_type {};

    struct data_type
    {
        bool initialised = false;
    };

    using actor_type::actor_type;

    static void system_tick(yoe::world_type& world)
    {
    }

    void initialise()
    {
        emplace<data_type>(true);
    }

    [[nodiscard]] bool is_initialised() const
    {
        return get<data_type>().initialised;
    }
};

SCENARIO("actor_type objects can be created")
{
    GIVEN("A world_type object")
    {
        yoe::world_type world;

        THEN("This world_type object can be used to create actor_type objects")
        {
            auto actor = world.create<test_actor_type>();
            REQUIRE(actor.is_initialised());
        }
    }
}

SCENARIO("actor_type objects can be destroyed")
{
    GIVEN("A world_type object")
    {
        yoe::world_type world;

        THEN("This world_type object can be used to create actor_type objects")
        {
            auto actor_a = world.create<test_actor_type>();
            auto actor_b = actor_a;

            REQUIRE(actor_a == actor_b);
            REQUIRE(actor_a.is_valid());
            REQUIRE(actor_b.is_valid());

            actor_a.destroy();

            REQUIRE_FALSE(actor_a.is_valid());
            REQUIRE_FALSE(actor_b.is_valid());

            AND_THEN("Destroying an invalid actor throws no exception")
            {
                REQUIRE_NOTHROW(actor_a.destroy());
                REQUIRE_NOTHROW(actor_b.destroy());
            }
        }
    }
}

SCENARIO("actor_type objects can can have parents")
{
    GIVEN("A world_type object")
    {
        yoe::world_type world;

        THEN("This world_type object can be used to create actor_type objects")
        {
            auto parent = world.create<test_actor_type>();
            auto child = world.create<test_actor_type>();

            THEN("A parent can be set")
            {
                child.set_parent(parent);

                REQUIRE(child.has_parent());
                REQUIRE_NOTHROW(child.get_parent());
                REQUIRE(child.get_parent() == parent);

                auto children = parent.get_children();
                REQUIRE(children.size() == 1);
                REQUIRE(children[0] == child);

                REQUIRE(child.get_children().empty());

                AND_THEN("The parent can be removed")
                {
                    child.remove_parent();
                    REQUIRE_FALSE(child.has_parent());
                    REQUIRE_THROWS_AS(child.get_parent(), yoe::actor_type::exception_type);
                    REQUIRE(parent.get_children().empty());
                }
            }
            THEN("A child can be set")
            {
                parent.add_child(child);

                REQUIRE(child.has_parent());
                REQUIRE_NOTHROW(child.get_parent());
                REQUIRE(child.get_parent() == parent);

                auto children = parent.get_children();
                REQUIRE(children.size() == 1);
                REQUIRE(children[0] == child);

                REQUIRE(child.get_children().empty());

                AND_THEN("Another child can be added")
                {
                    auto another_child = world.create<test_actor_type>();

                    parent.add_child(another_child);
                    REQUIRE(another_child.has_parent());
                    REQUIRE_NOTHROW(another_child.get_parent());
                    REQUIRE(another_child.get_parent() == parent);

                    children = parent.get_children();
                    REQUIRE(children.size() == 2);
                }
            }
        }
    }
}

class child_test_actor_type: public yoe::actor_type
{
public:
    struct tag_type {};

    using actor_type::actor_type;

    static void system_tick(yoe::world_type& world)
    {
    }

    void initialise()
    {
        _initialised = true;
    }

    [[nodiscard]] bool is_initialised() const
    {
        return _initialised;
    }

private:
    bool _initialised = false;
};

SCENARIO("actor_type objects have basic rtti")
{
    GIVEN("A world_type object")
    {
        yoe::world_type world;

        THEN("This world_type object can be used to create actor_type objects")
        {
            auto test = world.create<child_test_actor_type>();

            AND_THEN("The created actor object can be assigned to an actor_type")
            {
                yoe::actor_type actor = test;

                REQUIRE(actor == test);
                AND_THEN("The actor_type object still knows its real type")
                {
                    REQUIRE(actor.is_a<child_test_actor_type>());
                    REQUIRE_FALSE(actor.is_a<test_actor_type>());
                }
                AND_THEN("The actor_type object can be converted back to the right type")
                {
                    std::ignore = actor.as<child_test_actor_type>();
                }
                AND_THEN("Converting the actor_type object to a wrong type results in an exception")
                {
                    REQUIRE_THROWS_AS(actor.as<test_actor_type>(), yoe::actor_type::exception_type);
                }
            }
        }
    }
}

SCENARIO("actor_type objects have a transform")
{
    GIVEN("A world_type object")
    {
        yoe::world_type world;

        THEN("This world_type object can be used to create actor_type objects")
        {
            auto parent = world.create<test_actor_type>();

            AND_THEN("The transform can be set on the object")
            {
                parent.set_position({ 0.1f, 0.2f, 0.3f });
                parent.set_rotation({ 1.1f, 2.2f, 3.3f });
                parent.set_scale({ 2.2f, 2.4f, 3.6f });

                AND_THEN("The right values are returned")
                {
                    REQUIRE(parent.get_position() == yoe::components::position_type{ 0.1f, 0.2f, 0.3f });
                    REQUIRE(parent.get_rotation() == yoe::components::rotation_type{ 1.1f, 2.2f, 3.3f });
                    REQUIRE(parent.get_scale() == yoe::components::scale_type{ 2.2f, 2.4f, 3.6f });
                }
                AND_THEN("Without a hierarchy the world transform is not marked as dity")
                {
                    REQUIRE_FALSE(parent.all_of<yoe::components::dirty_world_transform_tag_type>());
                }
                AND_THEN("Without a hierarchy the local and the world positions are the same")
                {
                    REQUIRE(parent.get_world_position() == parent.get_position());
                }
            }
            AND_THEN("An actor hierarchy can be set up")
            {
                auto child_a = world.create<test_actor_type>();
                auto child_b = world.create<test_actor_type>();
                auto grandchild = world.create<test_actor_type>();
                parent.add_child(child_a);
                parent.add_child(child_b);
                child_a.add_child(grandchild);

                AND_THEN("Each actor can have a different local tranform")
                {
                    parent.set_position({ 1.1f, 2.2f, 3.3f });
                    child_a.set_position({ 4.4f, 5.5f, 6.6f });
                    grandchild.set_position({ 7.7f, 8.8f, 9.9f });

                    AND_THEN("Their world transform calculated correctly")
                    {
                        REQUIRE(parent.get_world_position() == yoe::components::position_type{ 1.1f, 2.2f, 3.3f });
                        REQUIRE(child_a.get_world_position() == yoe::components::position_type{ 5.5f, 7.7f, 9.9f });
                        REQUIRE(child_b.get_world_position() == yoe::components::position_type{ 1.1f, 2.2f, 3.3f });
                        REQUIRE(grandchild.get_world_position() == yoe::components::position_type{ 13.2f, 16.5f, 19.8f });
                    }
                }
            }
        }
    }
}
