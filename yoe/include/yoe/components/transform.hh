#ifndef TRANSFORM_HH
#define TRANSFORM_HH

namespace yoe::components
{
    struct position_type
    {
        static const position_type zero;

        [[nodiscard]] bool operator==(const position_type&) const = default;

        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
    };

    struct rotation_type
    {
        static const rotation_type zero;

        [[nodiscard]] bool operator==(const rotation_type&) const = default;

        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
    };

    struct scale_type
    {
        static const scale_type one;

        [[nodiscard]] bool operator==(const scale_type&) const = default;

        float x = 1.0f;
        float y = 1.0f;
        float z = 1.0f;
    };

    struct transform_type
    {
        static const transform_type identity;

        position_type position = {};
        rotation_type rotation = {};
        scale_type scale = {};
    };

    struct world_transform_cache_type
    {
        transform_type transform = {};
    };

    struct dirty_world_transform_tag_type {};
}

#endif //TRANSFORM_HH
