#ifndef HIERARCHY_HH
#define HIERARCHY_HH
#include <entt/entt.hpp>

#include <set>

namespace yoe::components
{
    struct hierarchy_type
    {
        entt::handle parent = {};
        std::set<entt::handle> children = {};
    };
}

#endif //HIERARCHY_HH
