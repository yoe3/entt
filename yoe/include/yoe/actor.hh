#ifndef ACTOR_HH
#define ACTOR_HH
#include <entt/entt.hpp>

#include "exception.hh"
#include "type_traits.hh"

namespace yoe
{
    namespace components
    {
        struct position_type;
        struct rotation_type;
        struct scale_type;
        struct transform_type;
    }

    class actor_type
    {
    public:
        class exception_type: public yoe::exception_type
        {
            using yoe::exception_type::exception_type;
        };

        explicit actor_type(entt::handle handle);
        void initialise();

        [[nodiscard]] bool operator==(const actor_type& other) const noexcept;
        [[nodiscard]] bool operator!=(const actor_type& other) const noexcept;

        template<system_type T>
        [[nodiscard]] bool is_a() const;
        template<system_type T>
        [[nodiscard]] T as() const;

        void set_parent(actor_type& parent);
        void remove_parent();
        [[nodiscard]] bool has_parent() const;
        actor_type get_parent();

        void add_child(actor_type& child);
        void remove_child(actor_type& child);
        [[nodiscard]] std::vector<actor_type> get_children() const;

        void set_position(const components::position_type& position);
        [[nodiscard]] const components::position_type& get_position() const;
        [[nodiscard]] const components::position_type& get_world_position() const;
        void set_rotation(const components::rotation_type& rotation);
        [[nodiscard]] const components::rotation_type& get_rotation() const;
        void set_scale(const components::scale_type& scale);
        [[nodiscard]] const components::scale_type& get_scale() const;

        template<typename T, typename... Types>
        T& emplace(Types... arguments)
        {
            return _handle.emplace<T>(arguments...);
        }

        template<typename T>
        const T& get() const
        {
            return _handle.get<T>();
        }

        template<typename... Types>
        [[nodiscard]] bool all_of() const
        {
            return _handle.all_of<Types...>();
        }

        [[nodiscard]] bool is_valid() const;

        void destroy();

    private:
        friend class world_type;

        void on_transform_updated();
        [[nodiscard]] const components::transform_type& get_world_transform() const;

        entt::handle _handle = {};
    };

    template<system_type T>
    bool actor_type::is_a() const
    {
        return _handle.all_of<typename T::tag_type>();
    }

    template<system_type T>
    T actor_type::as() const
    {
        if(!is_a<T>())
        {
            throw exception_type("Cannot convert base actor type to the requested type!");
        }

        return T(_handle);
    }

    static_assert(sizeof(actor_type) == sizeof(entt::handle), "actor_type should only contain an entt::handle");
}

#endif //ACTOR_HH
