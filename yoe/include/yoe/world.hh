#ifndef WORLD_HH
#define WORLD_HH
#include <entt/entt.hpp>
#include <set>
#include <algorithm>

#include "type_traits.hh"

namespace yoe
{
    class world_type
    {
    public:
        world_type();

        template<typename  T, typename... Types>
        T create(Types... arguments) requires initializable_system_type<T, Types...>
        {
            entt::handle handle(_registry, _registry.create());
            handle.emplace<typename T::tag_type>();

            T result(handle);
            result.initialise(std::forward<Types>(arguments)...);

            _system_ticks.emplace(&world_type::system_tick<T>);

            return result;
        }

        template<system_type T>
        std::vector<T> get_system_actors()
        {
            auto view = _registry.view<typename T::tag_type>();
            std::vector<T> result;

            std::transform(view.begin(), view.end(), std::back_inserter(result), [this](entt::entity entity)
            {
                return T(entt::handle(_registry, entity));
            });

            return result;
        }

        void tick();

    private:
        using system_tick_type = void(*)(world_type&);

        template<system_type T>
        static void system_tick(world_type& world)
        {
            T::system_tick(world);
        }

        static void on_transform_updated(entt::registry& registry, entt::entity entity);

        entt::registry _registry;
        std::set<system_tick_type> _system_ticks;
    };
}

#endif //WORLD_HH
