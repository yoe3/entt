#ifndef TYPE_TRAITS_HH
#define TYPE_TRAITS_HH
#include <type_traits>
//#include "actor.hh"

namespace yoe
{
    class world_type;

    template<typename T>
    concept default_constructible_empty_type = requires(T t)
    {
        requires std::is_default_constructible_v<T>;
        requires std::is_empty_v<T>;
    };

    template<typename T>
    concept system_type = requires(T t, world_type& world)
    {
        typename T::tag_type;
        requires default_constructible_empty_type<typename T::tag_type>;
        { T::system_tick(world) };
        // Do not require actor_type as base type
        // That would require us to include actor.hh,
        // and so actor_type would not be able to use these type traits
        //requires std::is_base_of_v<actor_type, T>;
        { T(entt::handle()) };
        // This would also require the inclusion of actor.hh
        // requires sizeof(T) == sizeof(actor_type);
    };

    template<typename T, typename... Types>
    concept initializable_system_type = requires(T t)
    {
        requires system_type<T>;
        { t.initialise(Types()...) };
    };
}

#endif //TYPE_TRAITS_HH
