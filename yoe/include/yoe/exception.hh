#ifndef EXCEPTION_HH
#define EXCEPTION_HH
#include <exception>
#include <string>

namespace yoe
{
    class exception_type: public std::exception
    {
    public:
        exception_type() = default;
        explicit exception_type(std::string message);

        [[nodiscard]] virtual const char* what() const noexcept override;

    private:
        std::string _message = {};
    };
}

#endif //EXCEPTION_HH
