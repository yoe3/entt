#include <yoe/world.hh>
#include <yoe/actor.hh>
#include <yoe/components/transform.hh>

yoe::world_type::world_type()
{
    _registry.on_update<components::transform_type>().connect<&world_type::on_transform_updated>();
}

void yoe::world_type::tick()
{
    auto view = _registry.view<components::dirty_world_transform_tag_type>();
    for(auto entity: view)
    {
        entt::handle handle(_registry, entity);
        // Just to update the cache
        std::ignore = actor_type(handle).get_world_transform();
    }
    for(auto system_tick: _system_ticks)
    {
        system_tick(*this);
    }
}

void yoe::world_type::on_transform_updated(entt::registry& registry, entt::entity entity)
{
    entt::handle handle(registry, entity);
    actor_type(handle).on_transform_updated();
}
