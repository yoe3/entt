#include <yoe/actor.hh>
#include <yoe/components/hierarchy.hh>
#include <yoe/components/transform.hh>

yoe::actor_type::actor_type(entt::handle handle):
    _handle(handle)
{
    // Nothing to do yet
}

bool yoe::actor_type::operator==(const actor_type& other) const noexcept
{
    return _handle == other._handle;
}

bool yoe::actor_type::operator!=(const actor_type& other) const noexcept
{
    return !operator==(other);
}

void yoe::actor_type::initialise()
{
    // Nothing to do yet
}

void yoe::actor_type::set_parent(actor_type& parent)
{
    auto& hierarchy = _handle.get_or_emplace<components::hierarchy_type>();

    // Remove ourselves for the current parent (if there is any)
    if(hierarchy.parent)
    {
        actor_type(hierarchy.parent).remove_child(*this);
    }
    // Add ourselves to the new parent
    parent.add_child(*this);
    // Set the new parent
    hierarchy.parent = parent._handle;
}

void yoe::actor_type::remove_parent()
{
    auto* hierarchy = _handle.try_get<components::hierarchy_type>();

    if(hierarchy == nullptr || !hierarchy->parent)
    {
        return;
    }

    actor_type(hierarchy->parent).remove_child(*this);
    hierarchy->parent = {};
}

bool yoe::actor_type::has_parent() const
{
    auto* hierarchy = _handle.try_get<components::hierarchy_type>();

    if(hierarchy == nullptr)
    {
        return false;
    }

    return static_cast<bool>(hierarchy->parent);
}

yoe::actor_type yoe::actor_type::get_parent()
{
    if(!has_parent())
    {
        throw exception_type("No parent found!");
    }

    auto& hierarchy = _handle.get<components::hierarchy_type>();

    return actor_type(hierarchy.parent);
}

void yoe::actor_type::add_child(actor_type& child)
{
    auto& hierarchy = _handle.get_or_emplace<components::hierarchy_type>();

    if(hierarchy.children.contains(child._handle))
    {
        return;
    }
    hierarchy.children.emplace(child._handle);
    child.set_parent(*this);
}

void yoe::actor_type::remove_child(actor_type& child)
{
    auto* hierarchy = _handle.try_get<components::hierarchy_type>();

    if(hierarchy == nullptr || !hierarchy->children.contains(child._handle))
    {
        return;
    }

    hierarchy->children.erase(child._handle);
    child.remove_parent();
}

std::vector<yoe::actor_type> yoe::actor_type::get_children() const
{
    auto* hierarchy = _handle.try_get<components::hierarchy_type>();

    if(hierarchy == nullptr)
    {
        return {};
    }

    return { hierarchy->children.begin(), hierarchy->children.end() };
}

bool yoe::actor_type::is_valid() const
{
    return static_cast<bool>(_handle);
}

void yoe::actor_type::destroy()
{
    if(_handle)
    {
        _handle.destroy();
        _handle = {};
    }
}

void yoe::actor_type::set_position(const components::position_type& position)
{
    std::ignore = _handle.get_or_emplace<components::transform_type>();

    _handle.patch<components::transform_type>([&position](components::transform_type& transform)
    {
        transform.position = position;
    });
}

const yoe::components::position_type& yoe::actor_type::get_position() const
{
    if(auto* transform = _handle.try_get<components::transform_type>(); transform != nullptr)
    {
        return transform->position;
    }

    return components::position_type::zero;
}

const yoe::components::position_type& yoe::actor_type::get_world_position() const
{
    return get_world_transform().position;
}

void yoe::actor_type::set_rotation(const components::rotation_type& rotation)
{
    std::ignore = _handle.get_or_emplace<components::transform_type>();

    _handle.patch<components::transform_type>([&rotation](components::transform_type& transform)
    {
        transform.rotation = rotation;
    });
}

const yoe::components::rotation_type& yoe::actor_type::get_rotation() const
{
    if(auto* transform = _handle.try_get<components::transform_type>(); transform != nullptr)
    {
        return transform->rotation;
    }

    return components::rotation_type::zero;
}

void yoe::actor_type::set_scale(const components::scale_type& scale)
{
    std::ignore = _handle.get_or_emplace<components::transform_type>();

    _handle.patch<components::transform_type>([&scale](components::transform_type& transform)
    {
        transform.scale = scale;
    });
}

const yoe::components::scale_type& yoe::actor_type::get_scale() const
{
    if(auto* transform = _handle.try_get<components::transform_type>(); transform != nullptr)
    {
        return transform->scale;
    }

    return components::scale_type::one;
}

void yoe::actor_type::on_transform_updated()
{
    if(auto* hierarchy = _handle.try_get<components::hierarchy_type>(); hierarchy == nullptr)
    {
        // No hierarchy, no need to mark world transform dirty
        return;
    }
    std::ignore = _handle.emplace_or_replace<yoe::components::world_transform_cache_type>();
    _handle.emplace_or_replace<yoe::components::dirty_world_transform_tag_type>();

    for(auto& child: get_children())
    {
        child.on_transform_updated();
    }
}

const yoe::components::transform_type& yoe::actor_type::get_world_transform() const
{
    auto* hierarchy = _handle.try_get<components::hierarchy_type>();
    auto* transform = _handle.try_get<components::transform_type>();

    // No hierarchy or no parent -> local transform is world transform
    if(hierarchy == nullptr || !hierarchy->parent)
    {
        if(transform != nullptr)
        {
            return *transform;
        }

        return components::transform_type::identity;
    }

    auto& world_transform = _handle.get_or_emplace<components::world_transform_cache_type>();

    // Trandform is not dirty -> return cached world transform
    if(!_handle.all_of<components::dirty_world_transform_tag_type>())
    {
        return world_transform.transform;
    }

    // Calculate new world transform
    const components::transform_type& parent_transform = actor_type(hierarchy->parent).get_world_transform();
    const components::transform_type& local_transform = transform == nullptr
        ? components::transform_type::identity
        : *transform;

    // Only position for now
    world_transform.transform.position.x = local_transform.position.x + parent_transform.position.x;
    world_transform.transform.position.y = local_transform.position.y + parent_transform.position.y;
    world_transform.transform.position.z = local_transform.position.z + parent_transform.position.z;

    // Remove dirty flag
    std::ignore = _handle.remove<components::dirty_world_transform_tag_type>();

    return world_transform.transform;
}
