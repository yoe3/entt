#include <yoe/components/transform.hh>

const yoe::components::position_type yoe::components::position_type::zero = {
    0.0f,
    0.0f,
    0.0f,
};

const yoe::components::rotation_type yoe::components::rotation_type::zero = {
    0.0f,
    0.0f,
    0.0f,
};

const yoe::components::scale_type yoe::components::scale_type::one = {
    1.0f,
    1.0f,
    1.0f,
};

const yoe::components::transform_type yoe::components::transform_type::identity = {
    position_type::zero,
    rotation_type::zero,
    scale_type::one,
};
