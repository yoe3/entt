#include <yoe/exception.hh>

yoe::exception_type::exception_type(std::string message):
    _message(std::move(message))
{
    // Nothing to do yet
}

const char* yoe::exception_type::what() const noexcept
{
    return _message.c_str();
}
