#include <yoe/world.hh>
#include <yoe/actor.hh>
#include <yoe/components/transform.hh>

#include <iostream>

class test_type: public yoe::actor_type
{
public:
    struct tag_type{};

    struct data_type
    {
        const char* name = nullptr;
    };

    using actor_type::actor_type;

    static void system_tick(yoe::world_type& world)
    {
        auto tests = world.get_system_actors<test_type>();

        for(auto test: tests)
        {
            auto children = test.get_children();

            std::cout << test.get_name() << " has " << children.size() << " children" << std::endl;
        }
    }

    void initialise(const char* name)
    {
        emplace<data_type>(name);
    }

    [[nodiscard]] const char* get_name() const
    {
        return get<data_type>().name;
    }
};

int main()
{
    yoe::world_type world;
    auto parent = world.create<test_type>("parent");
    auto child_a = world.create<test_type>("child_a");
    auto child_b = world.create<test_type>("child_b");
    auto grandchild = world.create<test_type>("grandchild");

    parent.add_child(child_a);
    parent.add_child(child_b);
    child_a.add_child(grandchild);

    parent.set_position({ 1.1f, 2.2f, 3.3f });

    world.tick();

    const yoe::components::position_type& world_position = grandchild.get_world_position();

    std::cout << "[ " << world_position.x << ", " << world_position.y << ", " << world_position.z << " ]" << std::endl;

    return 0.;
}